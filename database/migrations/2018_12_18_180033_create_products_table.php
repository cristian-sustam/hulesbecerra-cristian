<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique();
            $table->integer('image1_id');
            $table->integer('image2_id');
            $table->integer('image3_id');
            $table->integer('image4_id');
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->string('years_rank');
            $table->string('specifications');
            $table->string('motor');
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->integer('stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

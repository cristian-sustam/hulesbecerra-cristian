import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

//Componentes
//import Login from './components/admin/Login.vue';

const page="./components/page/";

const MyRouter = new VueRouter({
  	routes:[
      { path: '/', component: require(page+'home.vue'), meta:{title:"Home"}},
      { path: '/productos', component: require(page+'productos/index.vue'), meta:{title:"Productos"}},
	    { path: '/productos/detalle/', component: require(page+'productos/detalle.vue'), meta:{title:"Productos"}},
      { path: '/contacto', component: require(page+'contacto/index.vue'), meta:{title:"Contacto"}},
      { path: '/empresa', component: require(page+'empresa/index.vue'), meta:{title:"Empresa"}},
      { path: '/servicios', component: require(page+'servicios/index.vue'), meta:{title:"Servicios"}},
      { path: '/politicas-de-privacidad', component: require(page+'legal/politicas-de-privacidad.vue'), meta:{title:"Politicas de Privacidad"}},
	    // { path: '/checkout', component: require(page+'checkout.vue'), meta:{title:"Checkout"}},
	  ]
});

MyRouter.beforeEach((to, from, next) => {
	window.scrollTo(0,0);
	if(window.app.__vue__ && window.app.__vue__.$refs.loadingBar){
		window.app.__vue__.$refs.loadingBar.start();
	}
	next();
});

MyRouter.afterEach((to, from) => {

	if(window.app.__vue__ && window.app.__vue__.$refs.loadingBar){
		setTimeout(()=>{
			window.app.__vue__.$refs.loadingBar.done();
		},500);
	}


});

//Titulos del website
import VueDocumentTitlePlugin from "vue-document-title-plugin";
Vue.use(VueDocumentTitlePlugin, MyRouter,
	{ defTitle: "Home", filter: (title)=>{ return title+" - Hules Automotrices Becerra"; } }
);

// export {routes};
export default MyRouter;
